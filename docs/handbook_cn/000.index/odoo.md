## 强调重点

1. 了解 odoo 的设计逻辑. 需要忘记 你已知道的 类, 对象, 表, 关系, 外健 等概念
2. 如果你无法做到忘记上述名词. 那么你需要先做到, 把这些概念暂存. 先顺着 odoo 的设计思路理解 odoo
3. 初步熟悉了 odoo 中的名词之后, 再和上述名词对比. 认真分析异同. 一定要分析. 否则概念混淆. 全糊涂
4. 最后强调一句. 熟悉 odoo 不需要熟悉 python. odoo 中的名词概念, 不是 python 中的概念.
5. odoo 只是借用 python, 定义了自己的一套开发逻辑.
6. 因此 不要用 python 中的程序逻辑去理解 odoo

## 模型

### model, 模型的基本概念

1. 基本的业务管理单元
2. 数据存储角度来看, 雷同于 数据库的表
3. 程序运行角度, 雷同于 object
4. 模型名命名规范, 圆点分割的单词. 缺省对应的表名是 将模型名的圆点替换为下划线. 但有例外
5. 获取模型的语法. env[model_name].
6. env 是 odoo 中的一个工具.
7. 模型对外提供各种标准方法, 如 search, create, write, unlink
8. 模型可以拥有 本模型专用的方法.
9. 一个空模型, 如 obj = env['res.partner']. 其特点相当于一个 class
10. record = obj.search(), recordset = obj.create({}), 雷同于类方法.
11. 返回值 record 也是模型. 已经包含数据. 习惯上 odoo 称之为 结果集(recordset).
12. record 可以包含 0, 1, 或多条数据.
13. record 的多数方法 是集合操作. 也即 对多条记录进行处理.
14. 如 record.unlink() 的作用是 将 record 中的所有记录都删除.
15. record 的某些方法 只能对单条记录进行操作. 此时 要求 record 中有且仅有一条记录
16. 如 record.copy() 为单条记录方法
17. 访问 record 中的字段的值, 要求 record 必须是单条记录.
    若 record 为空, 则访问字段返回空.
    若 record 多余 1 条, 则访问字段, 返回第一条记录的字段的值(显然这不是合理的业务场景)
18.

### 模型的扩展(模型继承的其中之一)

1. odoo 模型的继承的第一种方式. 其作用是对已有模型进行扩展
2. 增加新字段
3. 增加新方法
4. 对已有字段进行修改. (修改字段的属性, 增加字段的属性)
5. 对已有方法进行修改. (等同于类继承中, 对方法的修改. 重写方法, 在执行父类方法的前后, 补充执行一些代码)

### 模型的继承(第 234 种方式, 基于已有模型, 定义新模型)

1. 模型继承, 除了上述一种, 对已有模型进行扩展之外还有另外两种. 这里称之为第二种, 第三种.
2. 第二种是基于已有一个或多个模型, 通过复制, 创建新模型,
   新模型继承已有模型的所有字段, 并可以定义新字段.
   多个旧模型的同名字段(等同于 python 的 class 继承中的同名属性), 如何被覆盖, 待验证.
   实际使用中, 被继承的多个模型中, 无重复的字段.
   在数据库表的对应上, 新模型将实现所有的字段.
   新模型可以定义新方法.
   新模型是否继承已有模型的方法. 待验证. 目前测试旧模型的方法将被继承.
   多个旧模型的同名方法, 如何先后调用, 待验证.
   实际使用中, 被继承的多个模型中, 无重复的方法.

3. 第三种是基于已有一个或多个模型, 通过引用, 创建新模型.
   新模型中, 需要定义一个 many2one 字段, 指向旧模型.
   基于多个旧模型, 则新模型中需要分别定义多个 many2one 字段.
   可以直接使用旧模型中的字段.
   多个旧模型的重名字, 如何处理. 待验证. 实际使用中, 未发现多个模型的继承情况.
   在数据库表的对应上, 旧模型对应表中的字段, 在新模型对应表中不再实现.
   旧模型的方法不能直接使用. 可以通过 many2one 字段 间接使用.

### 模型的定义

1. odoo 使用 python 语言开发
2. odoo 模型的定义使用 python 的 class
3. class 中一个关键的属性: \_name , 是 model 的 唯一名称
4. 第一种模型继承 -- 模型的扩展. 用到 class 的一个关键属性 \_inherit
5. 实现模型扩展, 定义 \_inherit 之外, 需要 \_name 同名或者不定义
6. 第二种模型继承 -- 模型复制, 需要 指定 \_inherit, 并保证 \_name 不同名
7. 第三种模型继承 -- 模型引用, 用到属性 \_inherits
8. 增加新字段, 或者修改旧字段, 直接在新类中定义即可. 注意修改旧字段, 不能修改字段的数据类型
9. 增加新方法, 或者修改旧方法, 直接在新类中定义即可.

## 模型的字段, field

1. 普通的字段类型. char, text, html, integer, float, monetary, boolean, date, datetime
2. 字段类型: integer, ID(integer 的特例)
3. 字段类型: json
4. 字段类型: properties, properties_definition
5. 字段类型: binary, Image(是 binary 的特例)
6. 字段类型: selection
7. 字段类型: many2one, reference, many2one_reference
8. 字段类型: many2many, one2many
9. 计算字段: 该字段的值是通过函数计算获得
10. 关联字段: 该字段的值关联到 many2one 字段对应模型的字段

## 模型的方法

1. 被 api.model 修饰过的方法是 空模型方法. 如 create, search 方法
2. 未被 api.model 修饰过的方法是 结果集方法. 如 write, unlink 方法
3. 单记录方法, 如 copy 方法
4. fields_get 方法
5. 读取数据的方法
6. onchange 方法

### fields_get 方法

1. 获取所有字段的方法: fields_get
2. odoojs 需要 对 fields_get 方法进行扩展. 新方法名为 load_metadata
3. load_metadata 相比 fields_get. 增加了 嵌套获取 子模型的 fields
4. 暂时 load_metadata 是在 odoojs 中实现的. 可考虑在 原生 odoo 中做补丁
5. fields_get 返回结果中的字段若为 many2one 或 many2many 类型. 则有 domain
6. domain 是数组结构或字符串结构. 若为数组结构, 可以直接使用
   若为字符串结构. 则后续需要通过计算生成数组. 字符串的语法为 python 语法
7. 暂时的处理手段是, 在 odoojs 中打补丁, 通过定义函数, 以表示每一个可能出现的需要计算的 domain.
8. 在官方 odoo 中有 py.js 代码库. 可以集成到 odoojs 中, 以处理字符串形式的 domain
9. 官方 odoo 的 py.js, 还有其他作用. 该方案需要 读懂 py.js 并删减重写后, 才可用.
10. 也可采取另一个方案. 在 odoo 中打补丁.
    将字符串格式的 domain, 解析后重新构建为 json 格式.
    然后在 odoojs 中处理 json 格式的 domain 字符串.
    该方案需要熟悉已有的 domain 字符串. 在服务端能够正确解析.
    并 自定义 json 格式的 domain 定义.

### 读取数据的方法

1. search_read, read, web_search_read 方法, 获取数据
2. odoojs 需要对上述三个方法, 进行扩展. 新方法命名为 web_load_data 和 load_data
3. web_load_data 和 load_data 方法, 一方面实现嵌套读取子模型的数据,
   另一方面, 对返回值结构进行标准化
4. 暂时 web_load_data, load_data 是在 odoojs 中实现的. 可考虑在 原生 odoo 中做补丁

### onchange 方法

1. onchange 是模型的一种特殊方法
2. onchange 方法处理模型的数据编辑
3. 编辑模型数据时, 某一字段的值发生变化, 将触发其他字段的相应变化.
   odoo 使用 onchange 方法实现该逻辑
4. onchange 方法的返回结果包括:
   其他字段变化后的新值, 以及其他 many2one 字段下拉选择项的 domain 变化
5. odoojs 需要 对 onchange 方法进行扩展. 新方法名为
6. call_onchange 方法, 对返回值结构进行标准化.
7. 暂时 call_onchange 是在 odoojs 中实现的. 可考虑在 原生 odoo 中做补丁

## action, view, menu

### 基本概念

1. menu 是页面中的菜单.
2. 点击 menu 触发 action
3. action 定义打开一个什么样的页面
4. view 定义页面的结构.
5. action, view, menu 作为程序控制数据, 本应该与业务数据分开.
   但是在官方 odoo 中, 混在同一个数据库中.
6. 在 odoojs 架构 中, 所有的 action, view, menu 自行定义.
   不再使用 官方 odoo 服务端的 action, view, menu

### menu

1. 官方 odoo 的 menu, 定义了 odoo 前端页面的所有菜单
2. 在 odoojs 架构中, menu 的定义直接与具体的业务密切相关.
   因此 menu 自行定义.
   点击 menu 后, 需要获得相应的 action

### action

1. 点击菜单, 触发一个 action. 或者 view 中 点击某个按钮, 也可触发一个 action
2. 一个 action 是对一个 model 的操作.
   如: 查询多条列表数据, 查询单条详情数据. 编辑单条数据
3. 在 action 中, 需要定义 domain, context, views
   views 包括 kanban, tree, form, search view 等
4. 查询多条数据用到 domain.
5. domain 是查询数据时的必选过滤条件
6. domain 格式为 数组或字符串.
7. domain 若为字符串, 则在触发时, 方可转换为数组. 转换时用到环境变量(如当前用户, 当前时间等)
8. 在 odoojs 中. domain 直接定义一个数组或函数即可.
9. 创建单条数据, 用到默认值, 在 context 中定义
10. 展示多条数据, 用到 treeview 或 kanbanview
11. treeview 或 kanbanview 中的搜索过滤功能, 用到 searchview
12. 展示单条数据, 用到 formview

### view

1. view 定义了一个页面的结构
2. 一个 view 可服务于多个 action. 故 view 的元素中 invisible 属性
3. 在 odoojs 中, 避免定义一个 view 服务于多个 action. 因此 invisible 的使用频率减少
4. 官方 odoo 的 view 定义中, 耦合了过多的 html 元素.
   该特点, 导致 odoojs 架构中直接使用官方 odoo 已经定义好的 view, 非常不便
5. 官方 odoo 的 view 可扩展.
   就是说, 多个模块中, 对同一个 model 有扩展定义. 则 view 也做扩展补充更新.
   选择是否安装某些模块, 则同一个 view 的元素不同.
   该特点, 进一步增加了 view 使用的难度.
6. 在 odoojs 中, view 重新定义.
   在不同的模块中, 定义不同的 action 及 view, 分别处理. 以减少代码的耦合性.
7. odoojs 架构中. 适配于 pc 端, 可直接模仿 官方 odoo 的 view 定义逻辑
8. odoojs 架构中. 适配于移动端,

## 模块 module

1. odoo 的模块是代码的组织形式, 一堆代码放在一起, 被称作一个模块
2. 一个模块, 可以定义若干 model, view, action, menu
3. 一个新模块, 可以对另外一个已有模块中定义的 model, 进行扩展
4. 一个新模块, 可以对另外一个已有模块中定义的 view, action, menu, 进行修改
5. 系统部署后, 模块可以选择是否安装.
6. 一个模块安装后, 将影响数据库表的结构
7. view, action, menu 的 ref_id 在模块内唯一. 加上模块名后, 全局唯一
8. 在 odoojs 中, 沿用了 module 的概念. 使用 module 组织所有的 view, action, menu
