## 基本概念

1. odoojs 是 js 代码库. 用在前端开发中.
2. 分为两部分. odoojs-rpc 和 odoojs-api
3. odoojs-rpc 管理所有的对 odoo 服务端的请求
4. odoojs-api 管理所有的前端自定义的 action, view, menu 以及 addons
5. odoojs-rpc 和 odoojs-api 已经发布为 npm 包.
6. 基于 antdv, 封装了一些组件. 适用于 pc 端开发.
7. 基于 uview-plus, 封装了一些组件. 适用于 uniapp 开发
8. 这些封装的组件, 有一点点方便处理 odoo 的功能. 是否可以封装为 ui-lib. 待议.
9. odoojs 架构, 这个词不严谨. 目前笼统的包括 上述内容以及官方 odoo
