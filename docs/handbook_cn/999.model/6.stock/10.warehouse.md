## stock.warehouse

### 属性

1. active
2. company_id
3. name
4. code

###

10. partner_id 地址
11. view_location_id 视图库位
12. lot_stock_id 物理库位
13. route_ids 该仓库的默认路由
14. rule_ids

###

1. reception_steps 收货方式 一步(入库)两步(暂存)三步(暂存/质检)
2. delivery_steps 送货方式 一步(出库)两步(暂存)三步(打包/暂存)
3. wh_input_stock_loc_id 入库暂存库位
4. wh_qc_stock_loc_id 质检库位
5. wh_output_stock_loc_id 出库暂存库位
6. wh_pack_stock_loc_id 打包库位
7. mto_pull_id MTO 拉规则
8. pick_type_id
9. pack_type_id 打包
10. out_type_id 出库
11. in_type_id 入库
12. int_type_id 内部
13. crossdock_route_id
14. reception_route_id
15. delivery_route_id
16. resupply_wh_ids 补货仓库
17. resupply_route_ids 补货路由

###
