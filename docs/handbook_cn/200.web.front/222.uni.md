### 移动端 使用 uniapp 创建项目

1. [uniapp](https://uniapp.dcloud.net.cn/)
2. [uniapp Vue3/Vite 版](https://gitee.com/dcloud/uni-preset-vue/repository/archive/vite.zip)
3. 使用 uniapp 提供的命令行方式创建失败的话, 直接下载该模板

```
npx degit dcloudio/uni-preset-vue#vite my-vue3-project
```

## 组件库使用 uview-plus

1. [uview-plus](https://uiadmin.net/uview-plus/)
2. [配置](https://uiadmin.net/uview-plus/components/downloadSetting.html)

```
yarn add uview-plus
yarn add sass --dev
yarn add sass-loader@10 --dev
```

## 在 main.js 添加 uviewPlus

```
import { createSSRApp } from 'vue'
import App from './App.vue'
import uviewPlus from 'uview-plus'

export function createApp() {
  const app = createSSRApp(App)
  app.use(uviewPlus)
  return {
    app
  }
}
```
