### 模型

1. odoojs 中的 Model. 是一个函数集合. 封装了所有的对 服务端 odoo 模型方法的调用
2. 通过 rpc.env.model('model_name') 获取一个模型
3. 之后可以使用模型方法

### odoojs 扩展的几个模型方法

1. load_metadata({ fields, context, callback })
2. load_web_data({domain, fields, offset, limit, order, context, callback})
3. load_data({domain, fields, offset, limit, order, context, callback})
4. load_data_one(res_id, {fields, context, callback})
5. load_metadata 方法获取模型中字段的属性
6. load_web_data, load_data, load_data_one 获取模型数据

### fields 参数说明

1. fields 参数, 指定获得哪些字段.
2. fields 可以是 Array 数组或 object 对象
3. fields 为 Array 数组时, 格式为:
   ['field_name', {name: 'relation_field_name', fields: [] or {} }, ...]
4. fields 为 Object 对象时, 格式为:
   {'field_name': null, 'relation_field_name': {fields: [] or {} }, ...}
5. field_name 为普通字段的字段名称.
6. relation_field_name 为 relation(many2one, one2many, many2many)类型字段的名称
7. relation 类型字段, 可以嵌套定义子模型的字段. 嵌套格式也是一个 fields 参数格式
8. relation 类型字段, 不需要嵌套时, 可以用普通字段的参数格式

### callback 参数说明

1. 因为这个几个方法, 由参数 fields 决定, 可能嵌套调用多次
2. callback 参数是一个函数, 该函数可以分多次返回 执行结果.
3. 若服务端 已安装 web_for_odoojs 模块,
   并且在 前端项目初始化 odoojs-rpc 中, 已配置 参数 run_by_server = true.
   则 callback 参数无意义

### 其他参数

1. domain 参数是过滤条件
2. order 参数是排序条件
3. offset, limit 参数, 控制分页
4. res_id. 数据记录 ID

### load_metadata 返回结果

1. 类同 odoo 的 fields_get. 但对 relation 类型字段有额外补充
2. relation 类型字段的返回结果中, 额外增加一个属性 metadata.
3. 内容为嵌套的子模型的 字段定义

### odoojs 数据集的标准化格式

#### 官方 odoo 的 jsonrpc 接口函数 search_read, read 返回结果

1. 结果为数组
2. 每个元素是一条记录的数据
3. one2many, many2many 类型字段 为空时, 数值为 []
4. 其他类型字段 为空时, 数值为 false
5. many2one 字段不为空时, 数值为 [id, display_name]
6. one2many, many2many 类型字段 不为空时, 数值为 [id1, id2, ...]

#### 官方 odoo 的 jsonrpc 接口函数 search_read, read 返回结果的不便之处

1. 以上数据格式在使用中, 存在以下不便:
2. 用 false 表示空值, 在前端代码中, 出现类型错误
3. many2one 字段 读写的格式不一致, 经常需要转换
4. one2many, many2many 的数据格式, 无法表示嵌套的字段数据

#### odoojs-rpc 的 load_data, load_web_data, load_data_one 的返回结果的标准化处理

1. many2one 类型字段, 数据格式转化为 {id, display_name} 支持嵌套读取额外字段
2. many2one 类型字段, 若为空值, 数值为 {id: null}
3. many2many 类型字段, 数据格式转化为 [{id, display_name, ...}] 支持嵌套读取额外字段
4. one2many 类型字段, 数据格式转化为 [{id, display_name, ...}] 支持嵌套读取额外字段
5. boolean 类型字段, 数值格式与官方相同
6. integer, float, monetary 若为空值, 标准化为 0 或 0.0
7. 其他类型的字段, 若无空值, 标准化为 null

### load_data 的返回结果

1. 类同 odoo 的 search_read.
2. 返回结果为数组 [{id, field1, field2, ...}]

### load_web_data 的返回结果

1. 类同 odoo 的 web_search_read.
2. 返回结果为数组 {length, records: [{id, field1, field2, ...}]}

### load_data_one 的返回结果

1. 返回结果为对象 {id, field1, field2, ...}
