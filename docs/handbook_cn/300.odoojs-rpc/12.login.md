### 获取 odoo 服务端的数据库列表

1. 登录页面. 需要用到服务端数据库列表.
2. 使用下述接口, 获取 服务端的数据库列表
3. 可以作为登录页面, 数据库选择框下拉列表的选择项

```
import { rpc } from '@/odoojs/index.js'
async function test_db_list() {
  const result = await rpc.web.database.list()
  console.log(result)
}
```

### 登录 odoo

1. 登录函数需要三个参数 db, login, password.
2. 登录参数, 由登录页面表单, 获得.
3. 登录成功后, odoojs-rpc 中, 自动处理 session, cokkie 等. 页面无需额外关心.
4. 登录成功后, 返回 session_info, 可用于显示登录用户信息
5. 在其他页面中, 也可以获得 session_info

```
import { rpc } from '@/odoojs/index.js'
async function test_login(){
  const db = 'your_db_name'
  const login = 'your_user_name'
  const password = 'your_password'
  const info = await rpc.login({ db, login, password })
  const info2 = rpc.web.session.session_info
}
```

### 检查登录状态

1. 页面跳转, 或者页面刷新时, 需要检查登录状态
2. 需要在路由守卫中添加 状态检查代码. 执行 session 检查.
3. 检查失败, 可以跳转到 登录页面

```
import { rpc } from '@/odoojs/index.js'

async function test_session_check(){
  const res = await rpc.session_check()

  if (res) {
    console.log('session check res:', res)
  } else {
    console.log('session error:')
    // 可以跳转到登录页面
  }
}
```

### 登出

1. 登出接口, 向 odoo 服务端发送请求.
2. odoo 服务端销毁 当前 session
3. 前端调用 登出接口之后, 应跳转到登录页面

```
import { rpc } from '@/odoojs/index.js'
async function test_logout() {
  await rpc.logout()
}
```
