import api from '../odoojs/index.js'

const db = 'T1'
const username = 'admin'
const password = '123456'

export async function login() {
  const payload = { db, login: username, password }
  const res = await api.login(payload)
  return res
}

export function sleep(millisecond) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve()
    }, millisecond)
  })
}
