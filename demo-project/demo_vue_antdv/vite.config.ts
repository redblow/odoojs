import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

import { fileURLToPath, URL } from 'node:url'

// https://vitejs.dev/config/
export default defineConfig({
  base: './', // 打包部署后, 查找静态文件用相对路径
  plugins: [vue()],
  resolve: {
    extensions: ['.js','.vue','.json','.ts'],
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
      // '@': resolve(__dirname, 'src')
    }
  },
  server: {
    port: 5183,
    proxy: {
      '/api': {
        target: 'http://127.0.0.1:8069', // o16

        changeOrigin: true,
        rewrite: path => path.replace(/^\/api/, '')
      }
    }
  }
})