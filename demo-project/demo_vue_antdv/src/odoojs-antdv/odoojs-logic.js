import {
  ONode,
  useWeb,
  useTree,
  useRead,
  useEdit,
  useNode,

  //
  useWOne2many,
  useO2mTree,
  useO2mRead,
  useO2mEdit,

  //
  useWMany2many,
  useM2mTree,
  useM2mRead,
  useM2mNew,
  //
  useWBoolean,
  useWSelection,
  //
  useWChar,
  useWText,
  useWHtml,
  useWImageUrl,
  useWImage,
  //
  useWInteger,
  useWFloat,
  useWMonetary,
  //
  useWDate,
  useWDatetime,
  //
  useWMany2one,
  useWM2mTags
  //
} from 'odoojs-api/lib/odoojs-vue'

// from '@/odoojs-vue'

export {
  ONode,
  useWeb,
  useTree,
  useRead,
  useEdit,
  useNode,
  //
  useWOne2many,
  useO2mTree,
  useO2mRead,
  useO2mEdit,
  //
  useWMany2many,
  useM2mTree,
  useM2mRead,
  useM2mNew,
  //
  useWBoolean,
  useWSelection,
  //
  useWChar,
  useWText,
  useWHtml,
  useWImageUrl,
  useWImage,
  //
  useWInteger,
  useWFloat,
  useWMonetary,
  //
  useWDate,
  useWDatetime,
  //
  useWMany2one,
  useWM2mTags
}
