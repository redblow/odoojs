### odoojs-vue

logic components for odoojs.

### odoojs-antdv

physic components for odoojs by ant-design-vue.

### odoojs-uview-plus

physic components for odoojs by uview-plus.

### addons_odoo

action and view for odoojs

### addons_menus_odoo

menu for odoojs

### contact us

contact us: odoojs@outlook.com
