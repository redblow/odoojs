import BaseLayout from '@/layout/BaseLayout.vue'

import { createRouter, createWebHistory } from 'vue-router'

import api from '@/odoojs/index'

const homeRoutes = [
  {
    path: '/',
    component: BaseLayout,
    redirect: '/home',

    children: [
      {
        path: '/home',
        component: () => import('@/views/HomeView.vue'),
        name: 'home'
      },

      {
        path: '/web',
        component: () => import('@/views/web/Web.vue'),
        name: 'web'
      }
    ]
  }
]

const userRoutes = [
  {
    path: '/web/login',
    name: 'web-login',
    component: () => import('@/views/LoginView.vue')
  }
]

const allRoutes = [...userRoutes, ...homeRoutes]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: allRoutes
})

router.beforeEach(async (to, from, next) => {
  // console.log(to, from)
  const whiteList = ['/web/login']
  if (whiteList.includes(to.path)) {
    next()
    return
  }

  const hasToken = await api.session_check()
  // const locallang = localStorage.getItem('lang') || lang_default
  // if (locallang) {
  //   await api.env.set_lang(locallang)
  // }
  if (hasToken) {
    next()
    return
  } else {
    next(`/web/login`)
    return
  }
})

export default router
