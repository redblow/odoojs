// axios 的标准配置
// 封装为函数
// 允许由 odoojs-jsonrpc 配置:
// 1. baseURL, timeout
// 2. 请求头中 配置 session_id
// 3. 截获响应头, 处理 session_id
// 4. 对响应结果做 额外处理
// 5. 对 响应 error, 自行处理

import axios from 'axios'

export class Request {
  constructor(payload) {
    const { baseURL, timeout = 500000, interceptors } = payload
    this._service = this._get_service({ baseURL, timeout, interceptors })
  }

  _get_service({ baseURL, timeout, interceptors }) {
    const service = axios.create({
      baseURL,
      withCredentials: true,
      timeout
    })

    service.interceptors.request.use(
      (config) => {
        // 允许 odoo jsonrpc 请求, 额外设置点东西
        return interceptors.request.use(config)
      },
      (error) => {
        // 允许 odoo jsonrpc 请求, 自行处理 error
        return interceptors.request.handleError(error)
      }
    )

    service.interceptors.response.use(
      (response) => {
        // 允许 odoo jsonrpc 请求, 额外处理响应结果
        return interceptors.response.use(response)
      },
      (error) => {
        // 允许 odoo jsonrpc 请求, 自行处理 error
        return interceptors.response.handleError(error)
      }
    )

    return service
  }

  call_post(url, data) {
    return this._service({ url, method: 'post', data })
  }
}
