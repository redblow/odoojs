import { UWebView } from '@/odoojs-antdv/index'

import { UNode, UNodeField, UNodeWidget } from '@/odoojs-antdv/index'

export { UWebView, UNode, UNodeField, UNodeWidget }
