import { Request } from './request_axios.js' 
import { OdooJSRPC } from 'odoojs-rpc'
import { OdooJSAPI } from 'odoojs-api'

import addons_actions_odoo from 'odoojs-api/lib/addons_actions_odoo/index.js'
import addons_menus_odoo from 'odoojs-api/lib/addons_menus_odoo/index.js'
 

function messageError(error) {
  console.log('error', error)
  console.log('show error', error.message)
  return Promise.reject(error)
}

// 开发时, 值为 '/api', 打包部署后, 值为 '/odoo'
const baseURL = import.meta.env.VITE_BASE_API
const timeout = 50000
const run_by_server = true // to set true if addons of web_for_odoojs installed

export const rpc = new OdooJSRPC({
  Request,
  baseURL,
  timeout,
  run_by_server,
  messageError
})

const addons_dict = {
  addons_actions_odoo,
  addons_menus_odoo
}

const modules_installed = [
  // 'base',
  // 'product',
  // 'analytic',
  // 'account',
  // 'contacts',
  // 'sale',
  // 'purchase'
]

const debug = 1
const api = new OdooJSAPI(rpc, { debug, addons_dict, modules_installed })

api.lang = 'zh_CN'
// api.lang = 'en_US'

export default api
