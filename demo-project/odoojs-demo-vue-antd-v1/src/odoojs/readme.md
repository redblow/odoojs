# help

1. odoojs-api 及 odoojs-rpc 的入口
2. 将 addons 作为参数 提供给 odoojs-api
3. addons_odoo 为官方 odoo 的标准功能
4. addons_l10n_zh_CN_odoo 为 addons_odoo 的中文翻译包
5. 如果有第三方其他的 模块. 自行定义 addons 即可
6. addons_menus_odoo 为 官方 odoo 标准功能 的菜单
7. addons_l10n_zh_CN_menus_odoo 为 addons_menus_odoo 的中文翻译包
8. 可自行定义其他菜单

# odoojs 项目基本结构

### 接口定义

1. 导入 odoojs-rpc
2. 导入 odoojs-api
3. 定义 addons 包括 action, view, menu. 及相应的语言翻译
4. 定义 odoojs/index.js. 初始化 odoojs-api 及 odoojs-rpc

### 流程

1. 定义 登录页面. 全局 获取 cookie
2. 渲染菜单 api.menus_tree
3. 点击菜单, 获取 action_id
4. 根据 action_id 跳转到 tree 页面
5. 在 tree 页面 选中一行, 跳转到 form 页面

### tree 页面

1. 根据 action_id 获取 action info
2. 获取数据
3. 获取 sheet
4. 渲染页面.
5. 使用 a-table 展示数据
6. 从 sheet 中, 取得 columns
7. 在 table 的 #bodyCell 插槽中, 使用组件: odoojs-antdv/OWidget/OdooWidget.vue

### form 页面

1. 根据 action_id 获取 action info
2. 根据 rid , 获取数据
3. 获取 sheet
4. 渲染页面
5. 使用 a-form 展示数据
6. 获取 sheet 的子节点.
7. 在 a-form 中, 使用组件: odoojs-antdv/ONode/TagNode.vue
8. TagNode 递归使用, 逐级渲染 所有的节点
9. 组件中 包括 group, notebook, button_box, div, field, label
10. 字段组件在 odoojs-antdv/OWidget 中
11. One2many 字段的渲染 用到的组件在 odoojs-antdv/OSubView
12. 编辑时用的组件, 在 odoojs-antdv/OInput
