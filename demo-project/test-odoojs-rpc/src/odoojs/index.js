import { Request } from 'odoojs-rpc/dist/request_axios.js'
import { OdooJSRPC } from 'odoojs-rpc'

function messageError(error) {
  console.log('show error', error.message)
  return Promise.reject(error)
}

// const baseURL = 'http://127.0.0.1:8069'
// const baseURL = 'http://192.168.56.101:8069' // odoo 16
const baseURL = 'http://192.168.56.103:8069' // odoo 17
const timeout = 50000
const run_by_server = true
export const rpc = new OdooJSRPC({
  Request,
  baseURL,
  timeout,
  // run_by_server,
  messageError
})
