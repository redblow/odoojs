import { rpc } from '../odoojs/index.js'
import { login } from './login.js'

const model_name = 'res.partner.category'
const fields = {
  name: {},
  color: {}
}

const Model = rpc.env.model(model_name)

const state = {
  reocrds: []
}

async function test_init() {
  // 创建三条测试数据, 用于运行这里的测试代码
  const resId1 = await Model.create({ name: 'TAG-Test1' })
  const resId2 = await Model.create({ name: 'TAG-Test2' })
  const resId3 = await Model.create({ name: 'TAG-Test3' })
  // 实际场景中 resId 通常来自于 页面参数

  const metadata = await Model.load_metadata({ fields })

  const { length, records } = await Model.load_web_data({
    fields,
    order: 'id desc'
  })

  state.reocrds = records
  console.log('length, records', length, records)
}

async function test_unlink() {
  const id1 = state.reocrds[0].id
  const id2 = state.reocrds[1].id
  const id3 = state.reocrds[2].id

  const ids = [id2, id3]

  await Model.unlink(ids)
  await Model.unlink(id1)

  const { length, records } = await Model.load_web_data({
    fields,
    order: 'id desc'
  })
  console.log('length, records', length, records)
}

async function test() {
  await login()
  await test_init()
  await test_unlink()
}

test()
