import { rpc } from '../odoojs/index.js'
async function test_version_info() {
  const result = await rpc.web.webclient.version_info()
  console.log('version_info', result)
  return result
}
test_version_info()

// 控制台 输出:
/*
const result = {
  server_version: '16.0-20230213',
  server_version_info: [16, 0, 0, 'final', 0, ''],
  server_serie: '16.0',
  protocol_version: 1
}
*/
