import { rpc } from '../odoojs/index.js'
import { login } from './login.js'

async function test_env() {
  const uid = rpc.env.uid
  const user = rpc.env.user
  const context = rpc.env.context
  const Model = rpc.env.model('res.users')

  console.log('uid', uid)
  console.log('user', user)
  console.log('context', context)
  console.log('Model', Model)
}

async function test_env_ref() {
  const xml_ref = 'base.user_admin'
  const ref_result = await rpc.env.ref(xml_ref)
  console.log('ref_result', ref_result)

  // 返回结果为:
  // const result = { model: 'res.users', id: 2 }
}

async function test_env_model() {
  const users_obj = rpc.env.model('res.users')
  const users_count = await users_obj.search_count([])
  console.log('users_count', users_count)
  const domain = [['active', 'in', [true, false]]]
  const users_count_all = await users_obj.search_count(domain)
  console.log('users_count_all', users_count_all)
}

async function test() {
  await login()
  await test_env()
  await test_env_ref()
  await test_env_model()
}

test()
