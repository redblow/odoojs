import { rpc } from '../odoojs/index.js'
import { login } from './login.js'

const model_name = 'res.country'
const fields = {
  name: {},
  code: {},
  state_ids: { fields: ['name', 'code'] }
}

async function test_load_data() {
  const Model = rpc.env.model(model_name)
  const metadata = await Model.load_metadata({ fields })
  const domain = [['code', 'like', 'C']]
  const order = ''
  const offset = 0
  const limit = 3
  const { length, records } = await Model.load_web_data({
    fields,
    domain,
    offset,
    limit,
    order
  })

  console.log('length, records', length, records)
}

async function test() {
  await login()
  await test_load_data()
}

test()
