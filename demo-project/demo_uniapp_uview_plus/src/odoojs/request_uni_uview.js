// uni.$u.http 的标准配置
// 封装为函数
// 允许由 odoojs-jsonrpc 配置:
// 1. baseURL, timeout
// 2. 请求头中 配置 session_id
// 3. 截获响应头, 处理 session_id
// 4. 对响应结果做 额外处理
// 5. 对 响应 error, 自行处理

export class Request {
  constructor(payload) {
    const { baseURL, timeout = 500000, interceptors } = payload

    uni.$u.http.setConfig(config => {
      /* config 为默认全局配置*/
      config.baseURL = baseURL /* 根域名 */
      return config
    })

    // 请求拦截
    uni.$u.http.interceptors.request.use(
      config => {
        // 允许 odoo jsonrpc 请求, 额外设置点东西
        return interceptors.request.use(config)
      },
      error => {
        // 允许 odoo jsonrpc 请求, 自行处理 error
        console.log('request error') // for debug
        return interceptors.request.handleError(error)
      }
    )

    // 响应拦截
    uni.$u.http.interceptors.response.use(
      response => {
        // 允许 odoo jsonrpc 请求, 额外处理响应结果
        return interceptors.response.use(response, response.header)
      },
      error => {
        // 允许 odoo jsonrpc 请求, 自行处理 error
        console.log('response error') // for debug
        return interceptors.response.handleError(error)
      }
    )
  }

  call_post(url, data) {
    const http = uni.$u.http
    return http.post(url, data)
  }
}
